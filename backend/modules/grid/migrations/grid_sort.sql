/*
SQLyog Ultimate v12.14 (64 bit)
MySQL - 5.6.34 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

CREATE TABLE `grid_sort` (
	`id` INT (11) PRIMARY KEY AUTO_INCREMENT,
	`visible_columns` TEXT ,
	`default_columns` TEXT ,
	`page_size` VARCHAR (300),
	`class_name` VARCHAR (300),
	`theme` VARCHAR (300),
	`label` VARCHAR (300),
	`user_id` INT (11)
	); 
