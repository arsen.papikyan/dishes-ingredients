<?php

namespace backend\controllers;

use Imagine\Image\Box;
use Yii;
use common\models\Dishes;
use backend\models\DishesControl;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DishesController implements the CRUD actions for Dishes model.
 */
class DishesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Dishes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DishesControl();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Dishes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Dishes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Dishes();

        if ($model->load(Yii::$app->request->post())) {
            $imgName = '';

            /* Upload Images */
            $imgFile = UploadedFile::getInstance($model, "img_name");

            if (!empty($imgFile)) {
                $imgPath = Yii::getAlias("@frontend") . "/web/images/dishes/";

                $imgName = Yii::$app->security->generateRandomString() . '.' . $imgFile->extension;
                $imgFile->saveAs($imgPath . $imgName);


                /**
                 * compress image, and  change size image
                 */
                $path = $imgPath . $imgName;
                $image = Image::getImagine()->open($path);

                $width = $image->getSize()->getWidth() >= 800 ? 800 : $image->getSize()->getWidth();
                $height = $image->getSize()->getHeight() >= 600 ? 600 : $image->getSize()->getWidth();

                $image->thumbnail(new Box($width, $height))
                    ->save($path, ['jpeg_quality' => 70])
                    ->save($path, ['png_compression_level' => 9]);


                /* small */

                $image = Image::getImagine()->open($path);

                $width = $image->getSize()->getWidth() >= 200 ? 200 : $image->getSize()->getWidth();
                $height = $image->getSize()->getHeight() >= 150 ? 150 : $image->getSize()->getWidth();

                $path = $imgPath . "small/" . $imgName;
                $image->thumbnail(new Box($width, $height))
                    ->save($path, ['jpeg_quality' => 40])
                    ->save($path, ['png_compression_level' => 9]);



            $model->img_name = $imgName;
        }
            if ($model->save()) {

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);

    }

    public function actionUploadImages($id)
    {
        $model = $this->findModel($id);

        $fullImgPath = Yii::getAlias("@frontend") . "/web/images/dishes/";

        if (!file_exists($fullImgPath)) {
            mkdir($fullImgPath, 0700);
        }
        if (!file_exists($fullImgPath . "small/")) {
            mkdir($fullImgPath . "small/", 0700);
        }

        $imgFile = UploadedFile::getInstance($model, "img_name");


        if (!empty($imgFile)) {

            if (!file_exists($fullImgPath . $model->img_name)) {
                unlink($fullImgPath . $model->img_name);
            }
            if (!file_exists($fullImgPath . "small/" . $model->img_name)) {
                unlink($fullImgPath . "small/" . $model->img_name);
            }

            $imgName = Yii::$app->security->generateRandomString() . '.' . $imgFile->extension;
            $imgFile->saveAs($fullImgPath . $imgName);


            /**
             * compress image, and  change size image
             */
            $path = $fullImgPath . $imgName;
            $image = Image::getImagine()->open($path);

            $width = $image->getSize()->getWidth() >= 800 ? 800 : $image->getSize()->getWidth();
            $height = $image->getSize()->getHeight() >= 600 ? 600 : $image->getSize()->getWidth();

            $image->thumbnail(new Box($width, $height))
                ->save($path, ['jpeg_quality' => 70])
                ->save($path, ['png_compression_level' => 9]);


            /* small */
            $image = Image::getImagine()->open($path);

            $width = $image->getSize()->getWidth() >= 200 ? 200 : $image->getSize()->getWidth();
            $height = $image->getSize()->getHeight() >= 150 ? 150 : $image->getSize()->getWidth();

            $path = $fullImgPath . "small/" . $imgName;
            $image->thumbnail(new Box($width, $height))
                ->save($path, ['jpeg_quality' => 40])
                ->save($path, ['png_compression_level' => 9]);


            $model->img_name = $imgName;
            if ($model->save(false)) {

                return true;
            }

        }
        return false;
    }

    public function actionDeleteFile($id)
    {

        $model = $this->findModel($id);

        if (!empty($model)) {

            $img_name = $model->img_name;
            $imgFullPath = Yii::getAlias("@frontend") . "/web/images/dishes/";


            if (file_exists($imgFullPath . $img_name)) {

                unlink($imgFullPath . $img_name);
            }
            if (file_exists($imgFullPath . "small/" . $img_name)) {
                unlink($imgFullPath . "small/" . $img_name);
            }
            $model->img_name = null;
            if ($model->save()) {

                return true;
            }
        }
        return false;
    }

    /**
     * Updates an existing Dishes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImgName = $model->img_name;

        if ($model->load(Yii::$app->request->post())) {
            $imgFile = UploadedFile::getInstance($model, "img_name");


            if (!empty($imgFile)) {
                $fullImgPath = Yii::getAlias("@frontend") . "/web/images/dishes/";


                if (file_exists($fullImgPath . $model->img_name)) {

                    unlink($fullImgPath . $model->img_name);
                }
                if (file_exists($fullImgPath . "small/" . $model->img_name)) {
                    unlink($fullImgPath . "small/" . $model->img_name);
                }

                $imgName = Yii::$app->security->generateRandomString() . '.' . $imgFile->extension;
                $imgFile->saveAs($fullImgPath . $imgName);


                /**
                 * compress image, and  change size image
                 */
                $path = $fullImgPath . $imgName;
                $image = Image::getImagine()->open($path);

                $width = $image->getSize()->getWidth() >= 800 ? 800 : $image->getSize()->getWidth();
                $height = $image->getSize()->getHeight() >= 600 ? 600 : $image->getSize()->getWidth();

                $image->thumbnail(new Box($width, $height))
                    ->save($path, ['jpeg_quality' => 70])
                    ->save($path, ['png_compression_level' => 9]);


                /* small */
                $image = Image::getImagine()->open($path);

                $width = $image->getSize()->getWidth() >= 200 ? 200 : $image->getSize()->getWidth();
                $height = $image->getSize()->getHeight() >= 150 ? 150 : $image->getSize()->getWidth();

                $path = $fullImgPath . "small/" . $imgName;
                $image->thumbnail(new Box($width, $height))
                    ->save($path, ['jpeg_quality' => 40])
                    ->save($path, ['png_compression_level' => 9]);


                $model->img_name = $imgName;
            }
            $model->img_name = $oldImgName;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing Dishes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Dishes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dishes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dishes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
