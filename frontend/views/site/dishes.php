<?php
/**
 * Created by PhpStorm.
 * User: Arsen
 * Date: 11/11/2017
 * Time: 4:15 AM
 */

use yii\grid\GridView;
use yii\helpers\Html;
use common\models\ConnectivityIngredients;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use common\models\Ingredients;
use yii\helpers\Url;
use yii\widgets\Pjax;


$dishes = ArrayHelper::map(\common\models\Dishes::find()
    ->select(['id', 'title'])
    ->where(['is_status' => true])
    ->asArray()
    ->all(),
    'title', 'title');

$ingredients = ArrayHelper::map(Ingredients::find()
    ->select(['id', 'title'])
    ->where(['is_status' => true])
    ->asArray()
    ->all(),
    'id', 'title');

Pjax::begin();
?>
<div class="col-xs-12">
    <div class="row">
       <?php

       echo GridView::widget([
           'dataProvider' => $dataProvider,
           'filterModel' => $searchModel,
           'tableOptions' => [
               'class' => 'table table-striped table-bordered'
           ],
           'columns' => [
               ['class' => 'yii\grid\SerialColumn'],

               [
                   'attribute' => 'img_name',
                   'format' => ['image', ['width' => '50', 'height' => '50']],
                   'value' => function ($data) {

                       return Yii::$app->homeUrl.'images/dishes/small/' . $data->img_name;

                   },
               ],
               [
                   'attribute' => 'title',
                   'filter' => Select2::widget([
                       'model'=>$searchModel,
                       'attribute' => "title",
                       'data' => $dishes,
                       'initValueText' => $searchModel->title,

                       'options' => [
                           'placeholder' => 'Dishes',
                       ],
                       'pluginOptions' => [
                           'allowClear' => true
                       ],
                   ])

               ],
               [
                   'attribute'=>     'short_description',
                   'format'=>'raw',
//    'contentOptions'=>[ 'class'=>'short_description'],
//            'headerOptions' => ['width' => '200px'],
//            'contentOptions'=>['style'=>'max-width: 200px;']
               ],

               [

                   'attribute' => 'ingredients',

                   'value' => function ($model) {
                       $temp = '';
                       $modelConnectivityIngredients = ConnectivityIngredients::find()
                           ->where(['dishes_id' => $model['id']])
                           ->asArray()
                           ->all();
                       $ingredientsId = array_column($modelConnectivityIngredients, 'ingredient_id');

                       $modelIngredients = Ingredients::find()
                           ->select(['id', 'title'])
                           ->where(['is_status' => true])
                           ->andWhere(['id' => $ingredientsId])
                           ->asArray()
                           ->all();
                       $lengthIngredients = count($modelIngredients) - 1;

                       foreach ($modelIngredients as $key => $val) {
                           if ($key != $lengthIngredients) {

                               $temp .= $val['title'] . ",  ";
                           } else {
                               $temp .= $val['title'] . " ";

                           }
                       }

                       return $temp;
                   },

                   'filter' => Select2::widget([
                       'model'=>$searchModel,
                       'attribute'=>'ingredients',
                       'data' => $ingredients,
                       'initValueText' => $searchModel->ingredients,
                       'options' => [
                           'multiple' => true,
                           'placeholder' => 'Ingredients',
                       ],
                       'pluginOptions' => [
                           'maximumInputLength' => 5,
                           'allowClear' => true
                       ],
                   ]) ,

               ],

               [
                   'class' => \yii\grid\ActionColumn::className(),
                   'buttons' => [
                       'view' => function ($url, $model) {
                           return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['site/dishes-view', 'id' => $model['id']]),
                               ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                       }
                   ],
                   'template' => '{view} ',
               ],
           ],
       ]);
Pjax::end();
       ?>
    </div>
</div>
