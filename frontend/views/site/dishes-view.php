<?php
/**
 * Created by PhpStorm.
 * User: Arsen
 * Date: 13/11/2017
 * Time: 4:01 AM
 */

?>

<div class="col-xs-12">
    <div class="row">

        <div class="col-xs-12 text-center" style="margin-bottom: 5em">
            <h1><?= $model['title'] ?></h1>
        </div>

        <div class="col-xs-12">

            <div class="col-xs-6">
                <img src="<?= Yii::$app->homeUrl ?>images/dishes/<?= $model['img_name'] ?>" width="300"
                     alt="<?= $model['title'] ?>">
            </div>
            <div class="col-xs-6 text-center ">
                <?= $model['content'] ?>
            </div>
        </div>
    </div>
</div>
