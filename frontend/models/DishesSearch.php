<?php

namespace frontend\models;


use common\models\ConnectivityIngredients;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Dishes;

/**
 * DishesControl represents the model behind the search form about `common\models\Dishes`.
 */
class DishesSearch extends Dishes
{
    public $ingredients;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_status'], 'integer'],
            [['title', 'content', 'short_description', 'description', 'keywords', 'updated_at', 'created_at', 'ingredients'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dishes::find()->where(['is_status' => 1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        if (!empty($params["DishesSearch"]['ingredients'])){

            $this->ingredients = $params["DishesSearch"]['ingredients'];
        }
        if (!empty($params["DishesSearch"]['title'])){

            $this->title = $params["DishesSearch"]['title'];
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if (!empty($this->ingredients)) {
            $ingredients = $this->ingredients;
            $dishesId = ConnectivityIngredients::find()
                ->where(['ingredient_id' => $ingredients])
                ->asArray()
                ->all();

            $dishesId = array_column($dishesId, 'dishes_id');
            $query->andFilterWhere(['id' => $dishesId]);

        }

        // grid filtering conditions
        $query->andFilterWhere([
            'is_status' => $this->is_status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'short_description', $this->short_description])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'keywords', $this->keywords])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at])
            ->andFilterWhere(['like', 'created_at', $this->created_at]);

        return $dataProvider;
    }
}
