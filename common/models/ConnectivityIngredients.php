<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "connectivity_ingredients".
 *
 * @property integer $id
 * @property integer $dishes_id
 * @property integer $ingredient_id
 *
 * @property Dishes $dishes
 * @property Ingredients $ingredient
 */
class ConnectivityIngredients extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'connectivity_ingredients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['dishes_id'], 'required'],
            [['dishes_id', 'ingredient_id'], 'integer'],
            [['dishes_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dishes::className(), 'targetAttribute' => ['dishes_id' => 'id']],
            [['ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredients::className(), 'targetAttribute' => ['ingredient_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dishes_id' => 'Dishes ID',
            'ingredient_id' => 'Ingredient ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDishes()
    {
        return $this->hasOne(Dishes::className(), ['id' => 'dishes_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredients::className(), ['id' => 'ingredient_id']);
    }
}
