<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "dishes".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $short_description
 * @property string $description
 * @property string $keywords
 * @property string $updated_at
 * @property string $created_at
 * @property string $img_name
 * @property integer $is_status
 *
 * @property ConnectivityIngredients[] $connectivityIngredients
 */
class Dishes extends ActiveRecord
{
    public $ingredients;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dishes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_status'], 'integer'],
            [['img_name','ingredients'], 'string'],
            [['title', 'content', 'short_description', 'keywords', 'updated_at', 'created_at'], 'string', ],
            [['description'], 'string', 'max' => 300],
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'is_status' => 'Is Status',
            'img_name' => 'Images',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConnectivityIngredients()
    {
        return $this->hasMany(ConnectivityIngredients::className(), ['dishes_id' => 'id']);
    }
}
